package euler

/**
  * Created by rogarcia on 29/04/16.
  */
object E2 extends App {

  var fibs = Vector[Int](1, 2)
  var fib: Int = 0
  var i = 2
  while (fib < 4000000) {
    fib = fibs(i - 1) + fibs(i -2)
    fibs = fibs :+ fib
    i = i + 1
  }

  println(fibs)
  val evenFibs = fibs.filter(_ % 2 == 0)
  println(evenFibs)
  println(evenFibs.sum)

}

package euler

/**
  * Created by rogarcia on 02/05/16.
  */
object E5 extends App {
  import Streams._
  import Math._

  val pff = (1L until 20L).map(i => primeFactors(i))

  val distPfs = pff.flatten.distinct

  val counts = distPfs.map (pf => pf -> pff.map(_.count(_ == pf)).max).toMap
  println(counts)

  val result = counts.map { case (x, y) => math.pow(x, y).toLong }.product
  println(result)
}

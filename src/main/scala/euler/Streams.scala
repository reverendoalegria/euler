package euler

/**
  * Created by rogarcia on 02/05/16.
  */
object Streams {
  def range(s: Long, e: Long): Stream[Long] = stream(s, _ + 1).takeWhile(_ <= e)
  def infrange(s: Long): Stream[Long] = stream(s, _ + 1)
  def revrange(s: Long, e: Long): Stream[Long] = stream(s, _ - 1).takeWhile(_ >= e)
  def stream(i: Long = 1, op: Long => Long): Stream[Long] = i #:: stream(op(i), op)
}

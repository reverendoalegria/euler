package euler

import scala.annotation.tailrec

/**
  * Created by rogarcia on 02/05/16.
  */
object Math {
  import Streams._

  def isPrime(n: Long): Boolean = {
    var count = 0
    range(2L, math.sqrt(n).toLong).takeWhile(_ => count < 3).foreach { d =>
      count = count + (if (n % d == 0) 1 else 0)
    }
    val isPrime = count == 0
    isPrime
  }

  def primes(n: Long): Stream[Long] = range(2L, n).filter(isPrime)

  def primeFactors(n: Long): Seq[Long] = {

    @tailrec
    def factor(d: Long, acc: Seq[Long]): Seq[Long] = {

      if (isPrime(d)) {
        acc :+ d
      } else {

        val div = primes(d).find(p => d % p == 0).get
        val rest = d / div

        if (isPrime(div)) {
          factor(rest, acc :+ div)
        } else /* if (isPrime(rest)) */ {
          factor(div, acc :+ rest)
        }

      }

    }

    factor(n, Seq())
  }

  def primeFactorsStream(n: Long): Stream[Long] = {

    def factor(d: Long): Stream[Long] = {

      if (isPrime(d)) {
        d #:: Stream.empty
      } else {

        val div = primes(d).find(p => d % p == 0).get
        val rest = d / div

        if (isPrime(div)) {
          div #:: factor(rest)
        } else /* if (isPrime(rest)) */ {
          rest #:: factor(div)
        }

      }

    }

    factor(n)
  }

}

object PrimeFactors extends App {
  import Math._

  println(primeFactorsStream(600851475143L).toList)

}

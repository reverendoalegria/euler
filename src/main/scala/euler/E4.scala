package euler

/**
  * Created by rogarcia on 02/05/16.
  */
object E4 extends App {

  def range(s: Long, e: Long): Stream[Long] = stream(s, _ + 1).takeWhile(_ <= e)
  def revrange(s: Long, e: Long): Stream[Long] = stream(s, _ - 1).takeWhile(_ >= e)
  def stream(i: Long = 1, op: Long => Long): Stream[Long] = i #:: stream(op(i), op)

  val palindroms = for {
    i <- revrange(999, 100)
    j <- revrange(999, 100)
    w = (i * j).toString
    if w == w.reverse
  } yield (i, j, w)

  println(palindroms.maxBy {
    case (i, j, w) => w.toLong
  })

}

package euler

import scala.annotation.tailrec

/**
  * Created by rogarcia on 29/04/16.
  */
object E3 extends App {

  def range(s: Long, e: Long): Stream[Long] = stream(s, _ + 1).takeWhile(_ <= e)
  def revrange(s: Long): Stream[Long] = stream(s, _ - 1).takeWhile(_ >= 0)

  def stream(i: Long = 1, op: Long => Long): Stream[Long] = i #:: stream(op(i), op)

  def isPrime(n: Long): Boolean = {
    var count = 0
    range(2L, math.sqrt(n).toLong).takeWhile(_ => count < 3).foreach { d =>
      count = count + (if (n % d == 0) 1 else 0)
    }
    val isPrime = count == 0
    isPrime
  }

  def primes(n: Long): Stream[Long] = range(2L, n).filter(isPrime)

  println(primeFactors(600851475143L))

  def primeFactors(n: Long): Seq[Long] = {

    @tailrec
    def factor(d: Long, acc: Seq[Long]): Seq[Long] = {

      if (isPrime(d)) {
        acc :+ d
      } else {

        val div = primes(d).find(p => d % p == 0).get
        val rest = d / div

        if (isPrime(div)) {
          factor(rest, acc :+ div)
        } else /* if (isPrime(rest)) */ {
          factor(div, acc :+ rest)
        }

      }

    }

    factor(n, Seq())
  }
}


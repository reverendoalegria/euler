package euler

object E1 extends App {

  val ns = for {
    i <- 1 until 1000
    if i % 3 == 0 || i % 5 == 0
  } yield i

  println(ns)
  println(ns.sum)

}
